include <stdio.h>
#include "sqlite3.h"
#include <iostream>
#include <string>
using namespace std;
int main() {
	const char* command;
	int rc;
	int temp;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;
	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	cout << "insert into people(name)values(\"Yoni\")"<<endl;
	rc = sqlite3_exec(db, "insert into people(name)values(\"Yoni\")",NULL, 0, &zErrMsg);
	cout << "insert into people(name)values(\"Vadim\")" << endl;
	rc = sqlite3_exec(db, "insert into people(name)values(\"Vadim\")", NULL, 0, &zErrMsg);
	cout << "insert into people(name)values(\"Ori\")" << endl;
	rc = sqlite3_exec(db, "insert into people(name)values(\"Ori\")", NULL, 0, &zErrMsg);
	cout << "SELECT last_insert_rowid()" << endl;
	rc = sqlite3_exec(db, "SELECT last_insert_rowid()", NULL, 0, &zErrMsg);
	temp = rc;
	cout << "update people set name=\"Uri\" where id=" << temp << endl;
	command = "update people set name=\"Uri\" where id=(select MAX(id) from people)";
	rc = sqlite3_exec(db, command, NULL, 0, &zErrMsg);
	system("CLS");
}