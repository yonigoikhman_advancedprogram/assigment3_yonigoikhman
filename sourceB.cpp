#include <stdio.h>
#include "sqlite3.h"
#include <iostream>
#include <string>
using namespace std;

int buyerMoney;
int costCar;
int carAvailable;
int fromMoney;
int toMoney;

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
int buyerMoneyCall(void *unused, int count, char **data, char **columns);
int costCarCall(void *unused, int count, char **data, char **columns);
int isCarAvailable(void *unused, int count, char **data, char **columns);
int fromMoneyCall(void *unused, int count, char **data, char **columns);
int toMoneyCall(void *unused, int count, char **data, char **columns);
int printWhoCanBuyCar(void *unused, int count, char **data, char **columns);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
void whoCanBuy(int carId, sqlite3* db, char* zErrMsg);


void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	string sql = "select price from cars where id=" + to_string(carId);
	sqlite3_exec(db, sql.c_str(), costCarCall, 0, &zErrMsg);
	sql = "select id from accounts where balance>" + to_string(costCar);
	//cout << endl << sql << endl;
	sqlite3_exec(db, sql.c_str(), printWhoCanBuyCar, 0, &zErrMsg);
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	string sql;
	sql = "select balance from accounts where id=" + to_string(from);
	sqlite3_exec(db, sql.c_str(), fromMoneyCall, 0, &zErrMsg);
	sql = "select balance from accounts where id=" + to_string(to);
	sqlite3_exec(db, sql.c_str(), toMoneyCall, 0, &zErrMsg);
	if (amount > fromMoney)
	{
		cout << "Money cannot be transfered,the sender doesn't have enough money." << endl;
		return false;
	}
	else {
		sql = "update accounts set balance= (case when id=" + to_string(from) + " then balance -" + to_string(amount) + " when id=" + to_string(to) + " then balance +" + to_string(amount) + " else balance end) where id in (" + to_string(from) +","+ to_string(to) + ")";
		sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);
		cout << "Money transfer completed!" << endl;
		return true;
	}
}

int printWhoCanBuyCar(void *unused, int count, char **data, char **columns)
{
	int idx;

	for (idx = 0; idx < count; idx++) {
		printf("ID's who can buy car are: %s\n", data[idx]);
	}
	printf("\n");
	return 0;
}

int toMoneyCall(void *unused, int count, char **data, char **columns)
{
	int a;
	int idx;
	for (idx = 0; idx < count; idx++) {
		a = atoi(data[idx]);
	}
	toMoney = a;
	return a;
}


int fromMoneyCall(void *unused, int count, char **data, char **columns)
{
	int a;
	int idx;
	for (idx = 0; idx < count; idx++) {
		a = atoi(data[idx]);
	}
	fromMoney = a;
	return a;
}


int isCarAvailable(void *unused, int count, char **data, char **columns)
{
	int a;
	int idx;
	for (idx = 0; idx < count; idx++) {
		a = atoi(data[idx]);
	}
	carAvailable = a;
	return a;
}

int costCarCall(void *unused, int count, char **data, char **columns)
{
	int a;
	int idx;
	for (idx = 0; idx < count; idx++) {
		a = atoi(data[idx]);
	}
	costCar = a;
	return a;
}

int buyerMoneyCall(void *unused, int count, char **data, char **columns)
{
	int a;
	int idx;
	for (idx = 0; idx < count; idx++) {
		a = atoi(data[idx]);
	}
	buyerMoney = a;
	return a;
}


bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	string sql;
	sql = "select available from cars where id=" + to_string(carid);
	sqlite3_exec(db, sql.c_str(), isCarAvailable, 0, &zErrMsg);
	sql = "select balance from accounts where id="+to_string(buyerid);
	sqlite3_exec(db, sql.c_str(), buyerMoneyCall, 0, &zErrMsg);
	sql = "select price from cars where id=" + to_string(carid);
	sqlite3_exec(db, sql.c_str(), costCarCall, 0, &zErrMsg);

	if (buyerMoney >= costCar&&carAvailable==1)
	{
		int newMoney = buyerMoney - costCar;
		sql = "update cars set available=0 where id=" + to_string(carid);
		sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);
		sql = "update accounts set balance=" + to_string(newMoney) + " where id=" + to_string(buyerid);
		sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);
		cout << "Purchase is done! Have fun!" << endl;
		return true;
	}
	else {
		cout << "Purchase couldn't be made"<<endl;
		return false;
	}
}


int main()
{
	sqlite3* db;
	char *zErrMsg = 0;
	int rc;
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	
	carPurchase(1, 7, db, zErrMsg);
	carPurchase(1,1, db, zErrMsg);
	carPurchase(12, 19, db, zErrMsg);
	balanceTransfer(2,1, 10000, db, zErrMsg);
	whoCanBuy(12, db, zErrMsg);
	system("pause");
}